Libreoffcie table to manage come/leave times at GSI/FAIR

As well provides some scripts to automatically add times to the table (e.g. on login/logout)

![screenshot](./static/screenshot.png)

For each month there is an overview, which can be copied 1:1 to the GSI "Zeiterfassungsbogen".

Check this page for info about this year's variable off-days:

https://www.gsi.de/work/administration/personalabteilung/arbeitszeitinformationen
